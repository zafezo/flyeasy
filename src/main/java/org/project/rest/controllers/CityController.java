package org.project.rest.controllers;

import org.project.core.entinity.City;
import org.project.core.services.CityService;
import org.project.core.utils.CityList;
import org.project.rest.resources.CityListResource;
import org.project.rest.resources.CityResource;
import org.project.rest.resources.asm.CityListResourceAsm;
import org.project.rest.resources.asm.CityResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by swen on 5/7/16.
 */
@Controller
@RequestMapping("/rest/cities")
public class CityController {

    private CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CityListResource> getAllCities(){
        CityList cityList = cityService.getAllCities();
        return new ResponseEntity<CityListResource>(new CityListResourceAsm().toResource(cityList), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CityResource> createCity(
            @RequestBody CityResource requestCity
    ) {
        City createCity = cityService.createCity(requestCity.toCity());
        return new ResponseEntity<CityResource>(
                new CityResourceAsm().toResource(createCity)
                ,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    public ResponseEntity<CityResource> updateCity(
            @RequestBody CityResource requestCity
    ) {
        City createCity = cityService.updateCity(requestCity.toCity());
        return new ResponseEntity<CityResource>(
                new CityResourceAsm().toResource(createCity)
                ,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<CityResource> deleteCity(
            @PathVariable Long id
    ) {
        City createCity = cityService.deleteCity(id);
        return new ResponseEntity<CityResource>(
                new CityResourceAsm().toResource(createCity)
                ,HttpStatus.CREATED);
    }
}
