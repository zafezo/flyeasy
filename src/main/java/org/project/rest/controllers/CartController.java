package org.project.rest.controllers;

import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.project.core.services.CartService;
import org.project.core.utils.CartList;
import org.project.core.utils.LineItemList;
import org.project.rest.resources.CartListResource;
import org.project.rest.resources.CartResource;
import org.project.rest.resources.LineItemListResource;
import org.project.rest.resources.LineItemResource;
import org.project.rest.resources.asm.CartListResourceAsm;
import org.project.rest.resources.asm.CartResourceAsm;
import org.project.rest.resources.asm.LineItemListResourceAsm;
import org.project.rest.resources.asm.LineItemResourceASm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;

/**
  * Created by swen on 4/23/16.
 */


@Controller
@RequestMapping("/rest/cart")
public class CartController {


    private CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CartResource> createCart(
     @RequestBody CartResource requestCart
    ){
        Cart createdCart =  cartService.createCart(requestCart.toCart());
        return new ResponseEntity<>(new CartResourceAsm().toResource(createdCart),HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CartListResource> getAllCarts(){
        return new ResponseEntity<>(
                new CartListResourceAsm()
                        .toResource(cartService.getAllCarts())
                ,HttpStatus.OK);
    }

    @RequestMapping(value = "/{cardId}/item",method = RequestMethod.POST)
    public ResponseEntity<LineItemResource> addLineItem(
            @PathVariable Long cardId,
            @RequestBody LineItemResource requestItem
    ){
        LineItem createdItem = cartService.addLineItem(cardId,requestItem.toLineItem());
        return new ResponseEntity<>(new LineItemResourceASm().toResource(createdItem), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{cardId}/items",method = RequestMethod.POST)
    public ResponseEntity<LineItemListResource> addLineItems(
            @PathVariable Long cardId,
            @RequestBody LineItemListResource requestItem
    ){

        LineItemListResource createdItems = new LineItemListResource();
        LineItemResourceASm asm = new LineItemResourceASm();
        for(LineItemResource item : requestItem.getLineItems()){
            createdItems.getLineItems()
                    .add(asm.toResource(cartService
                            .addLineItem(cardId,item.toLineItem())));
        }
        return new ResponseEntity<>(createdItems, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<LineItemListResource> getAllLineItemsByCartID(
            @PathVariable Long id
    ){
        return new ResponseEntity<>(new LineItemListResourceAsm().toResource(cartService.getLineItemsByCartId(id)), HttpStatus.OK);
    }


}
