package org.project.rest.controllers;

import org.project.core.entinity.Flight;
import org.project.core.services.FlightService;
import org.project.rest.resources.FlightListResource;
import org.project.rest.resources.FlightResource;
import org.project.rest.resources.ResultOfSearchResource;
import org.project.rest.resources.SearchFlightResource;
import org.project.rest.resources.asm.FlightListResourceAsm;
import org.project.rest.resources.asm.FlightResourceAsm;
import org.project.rest.resources.asm.ResultOfSearchResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by swen on 4/23/16.
 */
@Controller
@RequestMapping("/rest/flights")
public class FlightController {

    private FlightService flightService;


    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<FlightListResource> allFlight(){
        return new ResponseEntity<FlightListResource>(new FlightListResourceAsm().
                toResource(flightService.getAllFlight()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<FlightResource> createFlight(
            @RequestBody FlightResource requestFligt
    ){
        Flight createdFlight = flightService.createFlight(requestFligt.toFlight());
        return new ResponseEntity<>(new FlightResourceAsm().
                toResource(createdFlight), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    public ResponseEntity<FlightResource> updateFlight(
            @PathVariable Long id,
            @RequestBody FlightResource requestFligt
    ){
        Flight createdFlight = flightService.updateFlight(id,requestFligt.toFlight());
        return new ResponseEntity<>(new FlightResourceAsm().
                toResource(createdFlight), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<FlightResource> updateFlight(
            @PathVariable Long id
    ){
        Flight createdFlight = flightService.deleteFlight(id);
        return new ResponseEntity<>(new FlightResourceAsm().
                toResource(createdFlight), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST,value = "/search")
    public ResponseEntity<ResultOfSearchResource> getRequestedFlights(
            @RequestBody SearchFlightResource search
    ){
        ResultOfSearchResource res = new ResultOfSearchResourceAsm().toResource(flightService.getPossibleFlights(search));
        return new ResponseEntity<ResultOfSearchResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<FlightResource> getSpecificFlight(
            @PathVariable Long id
            ){
        return new ResponseEntity<FlightResource>(new FlightResourceAsm().
                toResource(flightService.findById(id)), HttpStatus.OK);
    }


}
