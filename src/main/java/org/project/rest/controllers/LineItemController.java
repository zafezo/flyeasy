package org.project.rest.controllers;

import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.project.core.services.LineItemService;
import org.project.rest.resources.CartResource;
import org.project.rest.resources.LineItemResource;
import org.project.rest.resources.asm.CartResourceAsm;
import org.project.rest.resources.asm.LineItemResourceASm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by swen on 5/10/16.
 */
@Controller
@RequestMapping("/rest/lineitems")
public class LineItemController {

    private LineItemService lineItemService;

    @Autowired
    public LineItemController(LineItemService lineItemService) {
        this.lineItemService = lineItemService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<LineItemResource> createCart(
         @RequestBody LineItemResource requestItem
    ){
        LineItem createdItem = lineItemService.createLineItem(requestItem.toLineItem());
        return new ResponseEntity<LineItemResource>(new LineItemResourceASm().toResource(createdItem), HttpStatus.CREATED);
    }

}
