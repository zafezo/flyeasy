package org.project.rest.controllers;

import org.project.core.entinity.Plane;
import org.project.core.services.PlaneService;
import org.project.rest.resources.PlaneListResource;
import org.project.rest.resources.PlaneResource;
import org.project.rest.resources.asm.PlaneListResourceAsm;
import org.project.rest.resources.asm.PlaneResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by swen on 5/26/16.
 */
@Controller
@RequestMapping("/rest/planes")
public class PlaneController {

    private PlaneService planeService;

    @Autowired
    public PlaneController(PlaneService planeService) {
        this.planeService = planeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PlaneListResource> getAllPlanes(){
        return new ResponseEntity<PlaneListResource>(
                new PlaneListResourceAsm().
                        toResource(planeService.getAllPlanes()),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PlaneResource> createPlane(
            @RequestBody PlaneResource requestPlane
    ) {
        Plane createPlane = planeService.createPlane(requestPlane.toPlane());
        return new ResponseEntity<PlaneResource>(
                new PlaneResourceAsm().toResource(createPlane)
                ,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    public ResponseEntity<PlaneResource> updatePlane(
            @RequestBody PlaneResource requestPlane
    ) {
        Plane createPlane = planeService.updatePlane(requestPlane.toPlane());
        return new ResponseEntity<PlaneResource>(
                new PlaneResourceAsm().toResource(createPlane)
                ,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<PlaneResource> deletePlane(
            @PathVariable Long id
    ) {
        Plane createPlane = planeService.deletePlane(id);
        return new ResponseEntity<PlaneResource>(
                new PlaneResourceAsm().toResource(createPlane)
                ,HttpStatus.CREATED);
    }

}
