package org.project.rest.resources;

import org.project.core.entinity.LineItem;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 5/12/16.
 */
public class LineItemListResource extends ResourceSupport {
    List<LineItemResource> lineItems;

    public LineItemListResource(){
        lineItems = new ArrayList<>();
    }

    public List<LineItemResource> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItemResource> lineItems) {
        this.lineItems = lineItems;
    }
}
