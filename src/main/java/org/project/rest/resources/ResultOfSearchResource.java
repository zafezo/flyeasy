package org.project.rest.resources;

import org.project.core.entinity.Flight;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 6/13/16.
 */
public class ResultOfSearchResource extends ResourceSupport {
    List<FlightListResource> result;

    public ResultOfSearchResource(List<FlightListResource> result) {
        this.result = result;
    }

    public List<FlightListResource> getResult() {
        return result;
    }

    public void setResult(List<FlightListResource> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResultOfSearchResource{" +
                "result=" + result +
                '}';
    }
}
