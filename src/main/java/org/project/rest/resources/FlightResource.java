package org.project.rest.resources;

import org.project.core.entinity.City;
import org.project.core.entinity.Flight;
import org.project.core.entinity.Plane;
import org.project.core.entinity.Route;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.DATE;

/**
 * Created by swen on 4/23/16.
 */
public class FlightResource extends ResourceSupport {

    private String to;
    private String from;
    private String price;

    private Long plane;
    private Date arriveDate;
    private Date  departureDate;

    private Long rid;

    public Long getPlane() {
        return plane;
    }

    public void setPlane(Long plane) {
        this.plane = plane;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Flight toFlight(){
        Flight flight = new Flight();
        flight.setIdflight(rid);
        City toCity = new City(to);
        City fromCity = new City(from);
        Route route = new Route();
            route.setFrom(fromCity);
            route.setTo(toCity);
            route.setPrice(Float.valueOf(price));
        flight.setRoute(route);
        flight.setDepartureDate(departureDate);
        flight.setArriveDate(arriveDate);
        Plane planeTemp = new Plane();
        planeTemp.setIdplane(plane);
        flight.setPlane(planeTemp);
        return flight;
    }
}
