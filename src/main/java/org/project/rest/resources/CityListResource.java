package org.project.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

/**
 * Created by swen on 5/7/16.
 */
public class CityListResource extends ResourceSupport {
    private List<CityResource> cities;

    public List<CityResource> getCities() {
        return cities;
    }

    public void setCities(List<CityResource> cities) {
        this.cities = cities;
    }
}
