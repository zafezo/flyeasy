package org.project.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 5/26/16.
 */
public class PlaneListResource extends ResourceSupport {
    List<PlaneResource> planes;

    public PlaneListResource() {
        planes = new ArrayList<>();
    }

    public List<PlaneResource> getPlanes() {
        return planes;
    }

    public void setPlanes(List<PlaneResource> planes) {
        this.planes = planes;
    }
}
