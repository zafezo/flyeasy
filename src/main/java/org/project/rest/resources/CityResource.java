package org.project.rest.resources;

import org.project.core.entinity.City;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by swen on 5/7/16.
 */
public class CityResource extends ResourceSupport {
    private Long rid;
    private String name;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City toCity(){
        City city = new City();
        city.setIdcity(rid);
        city.setName(name);
        return city;
    }
}
