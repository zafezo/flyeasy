package org.project.rest.resources;

import org.project.core.entinity.Plane;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by swen on 5/26/16.
 */
public class PlaneResource extends ResourceSupport {
    private Long rid;
    private String name;
    private int seats;

    public PlaneResource(){}

    public PlaneResource(Long rid, String name, int seats_count) {
        this.rid = rid;
        this.name = name;
        this.seats = seats_count;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Plane toPlane(){
        Plane temp = new Plane();
        temp.setIdplane(rid);
        temp.setName(name);
        temp.setSeats_count(seats);
        return temp;
    }
}
