package org.project.rest.resources;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.project.core.entinity.Flight;
import org.project.core.entinity.LineItem;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by swen on 5/10/16.
 */
public class LineItemResource extends ResourceSupport {
    private Long rid;
    private Long flightId;
    private int quantity;

    private String hashCode;

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public LineItem toLineItem() {
        LineItem temp = new LineItem();
        Flight flight = new Flight();
        flight.setIdflight(flightId);
        temp.setFlight(flight);
        temp.setQuantity(quantity);
        temp.setIdorder(rid);
        return temp;
    }
}
