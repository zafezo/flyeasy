package org.project.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/23/16.
 */
public class FlightListResource extends ResourceSupport {
    private List<FlightResource> flights;


    public FlightListResource() {
    }

    public FlightListResource(List<FlightResource> flightResourceList) {
        this.flights = flightResourceList;
    }

    public List<FlightResource> getFlights() {
        return flights;
    }

    public void setFlights(List<FlightResource> flights) {
        this.flights = flights;
    }
}
