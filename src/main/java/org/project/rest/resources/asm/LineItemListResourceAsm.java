package org.project.rest.resources.asm;

import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.project.core.utils.LineItemList;
import org.project.rest.controllers.CartController;
import org.project.rest.resources.LineItemListResource;
import org.project.rest.resources.LineItemResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/12/16.
 */
public class LineItemListResourceAsm extends ResourceAssemblerSupport<LineItemList, LineItemListResource> {
    public LineItemListResourceAsm() {
        super(CartController.class, LineItemListResource.class);
    }

    @Override
    public LineItemListResource toResource(LineItemList lineItemList) {
        LineItemListResource res = new LineItemListResource();
        res.setLineItems(new LineItemResourceASm().toResources(lineItemList.getLineItems()));
        return res;
    }
}
