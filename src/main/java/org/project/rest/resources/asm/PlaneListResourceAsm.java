package org.project.rest.resources.asm;

import org.project.core.utils.PlaneList;
import org.project.rest.controllers.PlaneController;
import org.project.rest.resources.PlaneListResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/26/16.
 */
public class PlaneListResourceAsm extends ResourceAssemblerSupport<PlaneList,PlaneListResource> {
    public PlaneListResourceAsm() {
        super(PlaneController.class, PlaneListResource.class);
    }

    @Override
    public PlaneListResource toResource(PlaneList planeList) {
        PlaneListResource temp = new PlaneListResource();
        temp.setPlanes(new PlaneResourceAsm().toResources(planeList.getPlanes()));
        return temp;
    }
}
