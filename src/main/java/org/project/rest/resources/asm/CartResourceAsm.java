package org.project.rest.resources.asm;

import org.project.core.entinity.Cart;
import org.project.rest.controllers.CartController;
import org.project.rest.resources.CartResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/25/16.
 */
public class CartResourceAsm extends ResourceAssemblerSupport<Cart,CartResource> {

    public CartResourceAsm() {
        super(CartController.class, CartResource.class);
    }

    @Override
    public CartResource toResource(Cart cart) {
        CartResource res = new CartResource();
        res.setFirst_name(cart.getFirst_name());
        res.setLast_name(cart.getLast_name());
        res.setRid(cart.getIdcart());
        res.setEmail(cart.getEmail());
        return res;
    }
}
