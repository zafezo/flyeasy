package org.project.rest.resources.asm;

import org.project.core.entinity.Flight;
import org.project.rest.controllers.FlightController;
import org.project.rest.resources.FlightResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 4/23/16.
 */
public class FlightResourceAsm extends ResourceAssemblerSupport<Flight,FlightResource> {
    public FlightResourceAsm() {
        super(FlightController.class, FlightResource.class);
    }

    @Override
    public FlightResource toResource(Flight entity) {
        FlightResource flightResource = new FlightResource();
        flightResource.setRid(entity.getIdflight());
        flightResource.setPrice(Float.toString(entity.getRoute().getPrice()));
        flightResource.setArriveDate(entity.getArriveDate());
        flightResource.setDepartureDate(entity.getDepartureDate());
        flightResource.setFrom(entity.getRoute().getFrom().getName());
        flightResource.setTo(entity.getRoute().getTo().getName());
        flightResource.setPlane(entity.getPlane().getIdplane());
        return flightResource;

    }
}
