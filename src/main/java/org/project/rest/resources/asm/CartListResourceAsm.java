package org.project.rest.resources.asm;

import org.project.core.utils.CartList;
import org.project.rest.controllers.CartController;
import org.project.rest.resources.CartListResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 6/17/16.
 */
public class CartListResourceAsm extends ResourceAssemblerSupport<CartList,CartListResource> {

    public CartListResourceAsm() {
        super(CartController.class, CartListResource.class);
    }

    @Override
    public CartListResource toResource(CartList entity) {
        CartListResource res = new CartListResource();
        res.setCarts(new CartResourceAsm().toResources(entity.getCarts()));
        return res;
    }
}
