package org.project.rest.resources.asm;

import org.project.core.utils.CityList;
import org.project.rest.controllers.CityController;
import org.project.rest.resources.CityListResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/7/16.
 */
public class CityListResourceAsm extends ResourceAssemblerSupport<CityList,CityListResource> {

    public CityListResourceAsm() {
        super(CityController.class, CityListResource.class);
    }

    @Override
    public CityListResource toResource(CityList entity) {
        CityListResource res = new CityListResource();
        res.setCities(new CityResourceAsm().toResources(entity.getCities()));
        return res;
    }
}
