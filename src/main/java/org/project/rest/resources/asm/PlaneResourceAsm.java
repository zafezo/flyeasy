package org.project.rest.resources.asm;

import org.project.core.entinity.Plane;
import org.project.rest.controllers.PlaneController;
import org.project.rest.resources.PlaneResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/26/16.
 */
public class PlaneResourceAsm extends ResourceAssemblerSupport<Plane,PlaneResource> {
    public PlaneResourceAsm() {
        super(PlaneController.class, PlaneResource.class);
    }

    @Override
    public PlaneResource toResource(Plane plane) {
        PlaneResource temp = new PlaneResource();
        temp.setRid(plane.getIdplane());
        temp.setName(plane.getName());
        temp.setSeats(plane.getSeats_count());
        return temp;
    }
}
