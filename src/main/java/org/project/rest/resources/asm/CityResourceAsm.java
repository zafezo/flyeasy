package org.project.rest.resources.asm;

import org.project.core.entinity.City;
import org.project.rest.controllers.CityController;
import org.project.rest.resources.CityResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 5/7/16.
 */
public class CityResourceAsm extends ResourceAssemblerSupport<City,CityResource> {

    public CityResourceAsm() {
        super(CityController.class, CityResource.class);
    }

    @Override
    public CityResource toResource(City entity) {
        CityResource res = new CityResource();
        res.setName(entity.getName());
        res.setRid(entity.getIdcity());
        return res;
    }
}
