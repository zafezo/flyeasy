package org.project.rest.resources.asm;

import org.project.core.entinity.Flight;
import org.project.core.utils.FlightList;
import org.project.rest.controllers.FlightController;
import org.project.rest.resources.FlightListResource;
import org.project.rest.resources.ResultOfSearchResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by swen on 6/13/16.
 */
public class ResultOfSearchResourceAsm extends ResourceAssemblerSupport<List<FlightList>, ResultOfSearchResource> {
    public ResultOfSearchResourceAsm() {
        super(FlightController.class, ResultOfSearchResource.class);
    }

    @Override
    public ResultOfSearchResource toResource(List<FlightList> flightLists) {
        return new ResultOfSearchResource(new FlightListResourceAsm().toResources(flightLists));
    }


}
