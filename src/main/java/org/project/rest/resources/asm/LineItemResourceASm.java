package org.project.rest.resources.asm;

import org.project.core.entinity.Flight;
import org.project.core.entinity.LineItem;
import org.project.rest.controllers.FlightController;
import org.project.rest.controllers.LineItemController;
import org.project.rest.resources.LineItemResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.apache.commons.lang3.builder.HashCodeBuilder;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by swen on 5/10/16.
 */
public class LineItemResourceASm extends ResourceAssemblerSupport<LineItem, LineItemResource> {
    public LineItemResourceASm() {
        super(LineItemController.class, LineItemResource.class);
    }

    @Override
    public LineItemResource toResource(LineItem item) {
        LineItemResource res = new LineItemResource();
        res.setQuantity(item.getQuantity());
        res.setFlightId(item.getFlight().getIdflight());
        res.setRid(item.getIdorder());
        res.setHashCode(createHashCode(item));
        res.add(linkTo(FlightController.class).slash(res.getFlightId()).withRel("flight"));
        return res;
    }


    private String createHashCode(LineItem item){
        StringBuilder temp = new StringBuilder();
        temp.append(new HashCodeBuilder(17, 37)
                .append(item.getFlight().getIdflight())
                .toHashCode());
        temp.append("-");
        temp.append(item.getIdorder());
        temp.append("-");
        temp.append(item.getFlight().getRoute().getFrom().getName().substring(0,3));
        temp.append("/");
        temp.append(item.getFlight().getRoute().getTo().getName().substring(0,3));
        return temp.toString();
    }
}
