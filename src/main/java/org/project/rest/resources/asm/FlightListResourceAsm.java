package org.project.rest.resources.asm;

import org.project.core.utils.FlightList;
import org.project.rest.controllers.FlightController;
import org.project.rest.resources.FlightListResource;
import org.project.rest.resources.FlightResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by swen on 4/23/16.
 */
public class FlightListResourceAsm extends ResourceAssemblerSupport<FlightList,FlightListResource>{

    public FlightListResourceAsm() {
        super(FlightController.class, FlightListResource.class);
    }

    @Override
    public FlightListResource toResource(FlightList entity) {
        FlightListResource res = new FlightListResource();
        res.setFlights(new FlightResourceAsm().toResources(
                entity.getFlights()));
        return res;
    }
}
