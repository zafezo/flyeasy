package org.project.rest.resources;

import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.springframework.hateoas.ResourceSupport;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by swen on 5/10/16.
 */
public class CartResource extends ResourceSupport {

    private Long rid;
    private String first_name;
    private String last_name;
    private String email;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public Cart toCart(){
        Cart temp = new Cart();
        temp.setFirst_name(first_name);
        temp.setLast_name(last_name);
        temp.setEmail(email);
        return temp;
    }
}
