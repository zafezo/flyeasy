package org.project.rest.resources;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by swen on 5/6/16.
 */
public class SearchFlightResource extends ResourceSupport {
    private Long to;
    private Long from;

    private Date startDate;
//    private Date  departureDate;


    @Override
    public String toString() {
        return "SearchFlightResource{" +
                "to=" + to +
                ", from=" + from +
                ", startDate=" + startDate +
                '}';
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

}
