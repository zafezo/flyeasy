package org.project.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

/**
 * Created by swen on 6/17/16.
 */
public class CartListResource extends ResourceSupport {
    List<CartResource> carts;

    public List<CartResource> getCarts() {
        return carts;
    }

    public void setCarts(List<CartResource> carts) {
        this.carts = carts;
    }
}
