package org.project.core.algo;

/**
 * Created by Khomenko_D on 22.04.2016.
 */
public class Edge {

    private final long firstFlightId;
    private final long secondFlightId;
    //   private boolean startCity=false;
    private boolean lastCity=false;


    public boolean isLastCity() {
        return lastCity;
    }

    public void setLastCity(boolean lastCity) {
        this.lastCity = lastCity;
    }

//   // public boolean isStartCity() {
//        return startCity;
//    }

//  //  public void setStartCity(boolean startCity) {
//        this.startCity = startCity;
//    }


    public Edge(Long firstFlightId, Long secondFlightId) {
        this.firstFlightId = firstFlightId;
        this.secondFlightId = secondFlightId;
        // this.transferTime = transferTime;
    }

    public Long getFirstFlightId() {
        return firstFlightId;
    }

    public Long getSecondFlightId() {
        return secondFlightId;
    }
}