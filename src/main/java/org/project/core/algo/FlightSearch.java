package org.project.core.algo;

import org.project.core.entinity.Flight;
import org.project.core.entinity.Route;
import org.project.core.services.FlightService;
import org.project.core.utils.FlightList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.*;

/**
 * Created by Khomenko_D on 22.04.2016.
 */

public class FlightSearch {


    private final static long MAX_DURATION_OF_TRAVEL_IN_DAYS = 3;
    private final static long TRIP_TIME_INTERVAL = (long) 86400000 * MAX_DURATION_OF_TRAVEL_IN_DAYS;
    private final static int MAX_RECURSION_DEPTH = 2; //from 0

    private FlightService flightService;


    private  List<Edge> edgeList = new ArrayList<>();
    //contain all Edges without Start Edges.

    private  HashMap<Long, Flight> flightsMap = new HashMap<Long, Flight>();
    //mapping Flight with Flight ID
    //contain flights which we will use to create our trip
    //used for not duplicate search of flight


    private  List<Long> idPreviousCity = new ArrayList<Long>();
    //contain id of city through which we move and to which must not return
    //used in EdgeCreator() and deleteReturnFlightsToPrevCities()


    private  Stack<Long> idPreviousFlight = new Stack<>();
    //contain id of Previous Flight for memorizing which Flight is before current
    //used in edgeCreator(long idCityFrom, Date startDate, Date maxArrivalDate, int counter)


    private  List<Edge> startFlights = new ArrayList<>();
    //contain all Edges that start in First city(CityFrom)

    private  List<List<Flight>> allPossibleFlights = new ArrayList<List<Flight>>();
    //contain List of List of Flight ( all possible flights from Start City to Last City
    // which will take place in some time interval that is equal TRIP_TIME_INTERVAL)

    private  long idStartCity = (long) 0;
    //id city from which we start our trip

    private  long idLastCity = (long) 0;
    //id city in which we end our trip


    @Autowired
    public FlightSearch(FlightService flightService) {
        this.flightService = flightService;
    }

    public List<List<Flight>> find(Route route, Date startDate) {
        //find all possible flights from Start City to Last City
        // which will take place in some time interval that is equal TRIP_TIME_INTERVAL
        //Parameters: 1)Route route - contain idStartCity(idCityFrom) and idLastCity(idCityTo)
        //2)Date startDate - contain time and date when the user wants to start journey
        //Return: List<List<Flight>> allPossibleFlights .

        Date maxArrivalDate = new Date(startDate.getTime() + TRIP_TIME_INTERVAL);

        List<Flight> flights = flightService.getByRouteAndStartDate(route, startDate, maxArrivalDate);

        if (!flights.isEmpty())
            for (Flight tempFlight : flights
                    ) {
                List<Flight> tempFlightList = new ArrayList<>();
                tempFlightList.add(tempFlight);


                allPossibleFlights.add(tempFlightList);
            }

        idStartCity = route.getFrom().getIdcity();
        idLastCity = route.getTo().getIdcity();

        edgeCreator(idStartCity, startDate, maxArrivalDate, 0);

        getPath();


        return allPossibleFlights;

    }


    private void getPath() {
        //search connections in List of Edges to create allPossibleFlights arraylist
        //if some connections is found, it add flights from edges to tempFlightList and than add it to allPossibleFlights

        if (!startFlights.isEmpty()) {

            for (Edge startEdge : startFlights) {

                long secondFlightID = startEdge.getSecondFlightId();

                if (startEdge.isLastCity()) {
                    List<Flight> tempFlightList = new ArrayList<>();

                    Flight firstFlight = flightsMap.get(startEdge.getFirstFlightId());
                    Flight secondFlight = flightsMap.get(startEdge.getSecondFlightId());

                    //Flight firstFlight = flightService.findById(startEdge.getFirstFlightId());
                    //Flight secondFlight = flightService.findById(startEdge.getSecondFlightId());

                    tempFlightList.add(firstFlight);
                    tempFlightList.add(secondFlight);

                    allPossibleFlights.add(tempFlightList);

                } else {     //need changes to recursion algorithm

                    List<Flight> tempFlightList = new ArrayList<>();
//                    Stack<Edge> tempEdgeStack =new Stack<>();
//                    List<Stack<Edge>> tempEdgeList=new ArrayList<>();
//                    recursionTripBuilder(tempEdgeStack , startEdge , tempEdgeList);
                    for (Edge edge : edgeList
                            ) {
                        if (edge.isLastCity()) {

                            boolean haveConnection = secondFlightID == edge.getFirstFlightId();

                            if (haveConnection) {

//                                Flight firstFlight = flightService.findById(startEdge.getFirstFlightId());
//                                Flight secondFlight = flightService.findById(startEdge.getSecondFlightId());
//                                Flight thirdFlight = flightService.findById(edge.getSecondFlightId());

                                Flight firstFlight = flightsMap.get(startEdge.getFirstFlightId());
                                Flight secondFlight = flightsMap.get(startEdge.getSecondFlightId());
                                Flight thirdFlight = flightsMap.get(edge.getSecondFlightId());

                                Flight[] flights = new Flight[]{firstFlight, secondFlight, thirdFlight};

                                Collections.addAll(tempFlightList, flights);

                                allPossibleFlights.add(tempFlightList);
                            }
                        }
                    }
                }//need changes to recursion algorithm
            }
        }
    }


//    private void recursionTripBuilder(Stack<Edge>  edgeStack , Edge startEdge , List<Stack<Edge>> tempEdgeList){
//
//
//        for (Edge edge: edgeList) {
//            boolean haveConnection=startEdge.getSecondFlightId()==edge.getFirstFlightId();
//
//           if(haveConnection)
//           {
//               edgeStack.push(edge);
//
//               if(edge.isLastCity()) {
//                   tempEdgeList.add(edgeStack);
//               }
//               else //need some codiction for break recursion , like price limit
//               recursionTripBuilder(edgeStack ,edge , tempEdgeList);
//           }
//
//        }
//    }


    private void deleteReturnFlightsToPrevCities(List<Flight> flightList, int counter) {
        //function that delete fligths from flightList that go to previous cities, in which we have already been
        //Parameters: 1) flightList- list of Flight from city(idCityFrom) and
        // which will take place in some time interval from Date startDate to Date maxArrivalDate).
        // are found by
        // flightService.getByCityFromIdAndStartDate(idCityFrom, startDate, maxArrivalDate)
        //2) counter - counter of recursion depth in edge Creator


        boolean isFlighListEmpty = flightList.isEmpty();
        boolean isZeroDepthOfRecurcion = counter == 0;//

        if (!isZeroDepthOfRecurcion && !isFlighListEmpty)//remove a back roads from second City to First  // morzlywo je prostisze riszennia
        {
            ArrayList<Flight> flightForRemove = new ArrayList<>();
            for (int i = 0; i < flightList.size(); i++) {
                for (Long l : idPreviousCity) {
                    long idCityTo1 = flightList.get(i).getRoute().getTo().getIdcity();
                    if (idCityTo1 == l) {
                        flightForRemove.add(flightList.get(i));
                    }

                }
            }
            for (Flight j : flightForRemove)
                flightList.remove(j);

        }
    }


    public void edgeCreator(long idCityFrom, Date startDate, Date maxArrivalDate, int counter) {
        //function that find all possible flights from idCityFrom and save it to edgeList
        //Parameters:
        //1)long idCityFrom - id city from which we start our trip
        //2)Date startDate - time and date of flight departure
        //3)Date maxArrivalDate - maximum of flight arrival time and date
        //4)int counter - counter of recursion depth


        List<Flight> flightList = flightService.getByCityFromIdAndStartDate(idCityFrom, startDate, maxArrivalDate);

        idPreviousCity.add(idCityFrom);

        deleteReturnFlightsToPrevCities(flightList, counter);

        for (Flight flight : flightList) {
            Date date = flight.getArriveDate();
            if (date.getTime() <= maxArrivalDate.getTime()) {

                Long idCityTo = flight.getRoute().getTo().getIdcity();

                Long flightId = flight.getIdflight();

                flightsMap.put(flightId, flight);


                if (!idPreviousFlight.isEmpty()) {

                    Edge edge = new Edge(idPreviousFlight.lastElement(), flightId);

                    boolean isStartCity = idPreviousFlight.size() == 1;

                    boolean isEndCity = idCityTo == idLastCity;

                    if (isEndCity)
                        edge.setLastCity(true);


                    if (isStartCity || (isStartCity && isEndCity)) {

                        //edge.setStartCity(true);

                        startFlights.add(edge);//add start City to StartFlights

                    } else {
                        //previousIdAndEdge.put(flightId, edge);
                        edgeList.add(edge);
                    }
                }


                boolean isItEndOfRecursion=(counter != MAX_RECURSION_DEPTH) && (idCityTo != idLastCity);

                if (isItEndOfRecursion) {
                    //previousFlight=flightId;

                    idPreviousFlight.push(flightId);

                    edgeCreator(idCityTo, date, maxArrivalDate, ++counter);

                    if (!idPreviousFlight.empty())
                        idPreviousFlight.pop();
                }
            }
        }

        counter = 0;
        idPreviousCity.clear();
    }
}
