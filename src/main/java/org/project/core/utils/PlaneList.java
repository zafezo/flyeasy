package org.project.core.utils;

import org.project.core.entinity.Plane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public class PlaneList {
    private List<Plane> planes = new ArrayList<>();

    public PlaneList(List resultList) {
        planes = resultList;
    }

    public List<Plane> getPlanes() {
        return planes;
    }

    public void setPlanes(List<Plane> planes) {
        this.planes = planes;
    }
}
