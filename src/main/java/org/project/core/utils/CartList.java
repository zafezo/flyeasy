package org.project.core.utils;

import org.project.core.entinity.Cart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 6/17/16.
 */
public class CartList {
    List<Cart> carts = new ArrayList<>();

    public CartList() {
    }

    public CartList(List<Cart> carts) {
        this.carts = carts;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }
}
