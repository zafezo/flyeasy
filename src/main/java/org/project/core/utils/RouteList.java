package org.project.core.utils;

import org.project.core.entinity.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public class RouteList {
    private List<Route> routes = new ArrayList<>();

    public RouteList(List resultList) {
        routes = resultList;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
