package org.project.core.utils;

import org.project.core.entinity.Flight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public class FlightList {
    private List<Flight> flights = new ArrayList<>();

    public FlightList(List resultList) {
        flights = resultList;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}
