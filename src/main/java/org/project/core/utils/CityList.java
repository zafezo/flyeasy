package org.project.core.utils;

import org.project.core.entinity.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public class CityList {
    private List<City> cities = new ArrayList<>();

    public CityList(List resultList) {
        cities = resultList;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
