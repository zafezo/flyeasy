package org.project.core.utils;

import org.project.core.entinity.LineItem;

import java.util.List;

/**
 * Created by swen on 5/12/16.
 */
public class LineItemList {
    List<LineItem> lineItems;

    public LineItemList(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }
}
