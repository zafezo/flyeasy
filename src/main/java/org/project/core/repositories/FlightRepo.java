package org.project.core.repositories;

import org.project.core.entinity.Flight;
import org.project.core.entinity.Route;
import org.project.core.utils.FlightList;

import java.util.Date;
import java.util.List;


/**
 * Created by swen on 4/22/16.
 */
public interface FlightRepo {
    List<Flight> findAllFlight();

    Flight findById(Long id);
    Flight createFlight(Flight flight);
    Flight updateFlight(Flight flight);
    Flight deleteFlight(Flight flight);

    List<Flight> findByRouteAndStartDate(Route route, Date date, Date maxArrivalDate);

    Flight findByRouteIdAndTimeInterval(Long routeId, Date minStartDate,Date maxArrivalDate);
}
