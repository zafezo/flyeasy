package org.project.core.repositories;

import org.project.core.entinity.City;
import org.project.core.entinity.Route;
import org.project.core.utils.RouteList;

import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public interface RouteRepo{
    RouteList findAllRoutes();
    Route findById(Long id);
    Route findByToCity(City to);
    Route findByFromCity(City from);
    Route findByFromAndToCity(City to, City from);
    Route findByFromAndToCity(Long to, Long from);
    Route createRoute(Route route);
    List<Long> findIdsRoutesByIdCityFrom(Long idCityFrom);
}
