package org.project.core.repositories;

import org.project.core.entinity.LineItem;

import java.util.List;

/**
 * Created by swen on 5/9/16.
 */
public interface LineItemRepo {
    LineItem createLineItem(LineItem item);
    LineItem updateLineItem(LineItem item);
    LineItem deleteLineItem(LineItem item);
    LineItem findById(Long id);
    List<LineItem> findByCartId(Long id);
}
