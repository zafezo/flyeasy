package org.project.core.repositories;

import org.project.core.entinity.City;
import org.project.core.utils.CityList;

import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public interface CityRepo {
    List<City> findAllCity();
    City findCity(Long id);
    City updateCity(Long id, City data);
    City deletCity(Long id);
    City createCity(City data);
    City findByName(String name);
}
