package org.project.core.repositories;

import org.project.core.entinity.Cart;
import org.project.core.utils.CartList;

import java.util.List;

/**
 * Created by swen on 5/9/16.
 */
public interface CartRepo {
    Cart createCart(Cart cart);
    Cart deleteCart(Cart cart);
    Cart updateCart(Cart cart);
    Cart findById(Long cartId);
    List<Cart> findAllCarts();
}
