package org.project.core.repositories;

import org.project.core.entinity.Plane;
import org.project.core.utils.PlaneList;

import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
public interface PlaneRepo {
    List<Plane> findAll();
    Plane findPlane(Long id);
    Plane updatePlane(Long id, Plane data);
    Plane deletPlane(Long id);
    Plane createPlane(Plane data);
}
