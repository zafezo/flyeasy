package org.project.core.repositories.jpa;

import org.project.core.entinity.Plane;
import org.project.core.repositories.PlaneRepo;
import org.project.core.utils.PlaneList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
@Repository
public class JpaPlaneRepo implements PlaneRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Plane> findAll() {
        Query query = em.createQuery("select p from Plane p");
        return  query.getResultList();
    }

    @Override
    public Plane findPlane(Long id) {
        return em.find(Plane.class, id);
    }

    @Override
    public Plane updatePlane(Long id, Plane data) {
        Plane plane = findPlane(id);
        plane.setName(data.getName());
        plane.setSeats_count(data.getSeats_count());
        return plane;
    }

    @Override
    public Plane deletPlane(Long id) {
        Plane plane = findPlane(id);
        em.remove(plane);
        return plane;
    }

    @Override
    public Plane createPlane(Plane data) {
        em.persist(data);
        return data;
    }
}
