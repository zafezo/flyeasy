package org.project.core.repositories.jpa;

import org.project.core.entinity.City;
import org.project.core.repositories.CityRepo;
import org.project.core.utils.CityList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
@Repository
public class JpaCityRepo implements CityRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<City> findAllCity() {
        Query query = em.createQuery("select c from City c");
        return query.getResultList();
    }

    @Override
    public City findCity(Long id) {
        return em.find(City.class, id);
    }

    @Override
    public City updateCity(Long id, City data) {
        City city = findCity(id);
        city.setName(data.getName());
        return city;
    }

    @Override
    public City deletCity(Long id) {
        City city = findCity(id);
        em.remove(city);
        return city;
    }

    @Override
    public City createCity(City data) {
        em.persist(data);
        return data;
    }

    @Override
    public City findByName(String name) {
        Query query = em.createQuery("select c from City c where c.name=?1");
        query.setParameter(1,name);
        System.out.println(name);
        List<City> cities = query.getResultList();
        if(!cities.isEmpty()){
            return cities.get(0);
        }
        return null;
    }
}
