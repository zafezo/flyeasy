package org.project.core.repositories.jpa;

import org.project.core.entinity.Flight;
import org.project.core.entinity.Route;
import org.project.core.repositories.FlightRepo;
import org.project.core.utils.FlightList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
@Repository
public class JpaFlightRepo implements FlightRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Flight> findAllFlight() {
        Query query = em.createQuery("select f from Flight f");
        return  query.getResultList();
    }

    @Override
    public Flight findById(Long id) {
        return em.find(Flight.class, id);
    }

    @Override
    public Flight createFlight(Flight flight) {
        em.persist(flight);
        return flight;
    }

    @Override
    public Flight updateFlight(Flight flight) {
        Flight flight1 = findById(flight.getIdflight());
        flight1.setRoute(flight.getRoute());
        flight1.setArriveDate(flight.getArriveDate());
        flight1.setDepartureDate(flight.getDepartureDate());
        flight1.setPlane(flight.getPlane());
        return flight1;
    }

    @Override
    public Flight deleteFlight(Flight flight) {
        em.remove(flight);
        return flight;
    }
    @Override
    public List<Flight> findByRouteAndStartDate(Route route, Date startDate , Date maxArrivalDate) {//Date (departure Date) + time Interval
        Query query = em.createQuery("select f from Flight f  where f.route=?1 and f.departureDate>=?2 and f.arriveDate<=?3");

        query.setParameter(1,route);
        query.setParameter(2,startDate);
        query.setParameter(3,maxArrivalDate);

        return new ArrayList<>(query.getResultList());
    }

    @Override
    public Flight findByRouteIdAndTimeInterval(Long routeId, Date minStartDate , Date maxArrivalDate){
        Query query = em.createQuery("select f from Flight f where (f.route.idroute=?1 and f.departureDate>=?2 and" +
                " f.arriveDate<=?3) order by f.departureDate");
        query.setParameter(1,routeId);
        query.setParameter(2,minStartDate);
        query.setParameter(3,maxArrivalDate);

        List<Flight> flights = query.getResultList();
        if(flights.isEmpty()){
            return null;
        }else {
            return flights.get(0);
        }
    }
}
