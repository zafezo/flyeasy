package org.project.core.repositories.jpa;

import org.project.core.entinity.Cart;
import org.project.core.entinity.City;
import org.project.core.repositories.CartRepo;
import org.project.core.utils.CartList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by swen on 5/10/16.
 */
@Repository
public class JpaCartRepo implements CartRepo{

    @PersistenceContext
    EntityManager em;

    @Override
    public Cart createCart(Cart cart) {
        em.persist(cart);
        return cart;
    }

    @Override
    public Cart deleteCart(Cart cart) {
        em.remove(cart);
        return cart;
    }

    @Override
    public Cart updateCart(Cart cart) {
        Cart c = em.find(Cart.class,cart.getIdcart());
        c.setEmail(cart.getEmail());
        c.setFirst_name(cart.getFirst_name());
        c.setLast_name(cart.getLast_name());
        c.setLineItems(cart.getLineItems());
        return c;
    }

    @Override
    public Cart findById(Long cartId) {
        return em.find(Cart.class,cartId);
    }

    @Override
    public List<Cart> findAllCarts() {
        Query query = em.createQuery("select c from Cart c");
        return query.getResultList();
    }

}
