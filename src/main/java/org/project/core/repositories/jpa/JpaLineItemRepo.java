package org.project.core.repositories.jpa;

import org.project.core.entinity.LineItem;
import org.project.core.repositories.LineItemRepo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by swen on 5/10/16.
 */
@Repository
public class JpaLineItemRepo implements LineItemRepo {


    @PersistenceContext
    private EntityManager em;

    @Override
    public LineItem createLineItem(LineItem item) {
        em.persist(item);
        return item;
    }

    @Override
    public LineItem updateLineItem(LineItem item) {
        LineItem lm = em.find(LineItem.class,item.getIdorder());
        lm.setFlight(item.getFlight());
        lm.setQuantity(item.getQuantity());
        return lm;
    }

    @Override
    public LineItem deleteLineItem(LineItem item) {
        em.remove(item);
        return item;
    }

    @Override
    public LineItem findById(Long id) {
        return em.find(LineItem.class,id);
    }

    @Override
    public List<LineItem> findByCartId(Long id) {
        Query query = em.createQuery("select l from LineItem l join l.carts c where c.idcart=?1");
        query.setParameter(1,id);
        return query.getResultList();
    }
}
