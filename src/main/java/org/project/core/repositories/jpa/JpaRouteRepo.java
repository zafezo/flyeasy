package org.project.core.repositories.jpa;

import org.project.core.entinity.City;
import org.project.core.entinity.Route;
import org.project.core.repositories.RouteRepo;
import org.project.core.utils.FlightList;
import org.project.core.utils.RouteList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 4/22/16.
 */
@Repository
public class JpaRouteRepo implements RouteRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public RouteList findAllRoutes() {
        Query query = em.createQuery("select r from Route r");
        return  new RouteList(query.getResultList());
    }

    @Override
    public Route findById(Long id) {
        return em.find(Route.class, id);
    }

    @Override
    public Route findByToCity(City to) {
        Query query = em.createQuery("select r from Route r where r.idcity_to=?1");
        query.setParameter(1,to.getIdcity());
        List<Route> resultList = query.getResultList();
        if(resultList.isEmpty()){
            return null;
        }else {
            return resultList.get(0);
        }
    }

    @Override
    public Route findByFromCity(City from) {
        Query query = em.createQuery("select r from Route r where r.from.idcity=?1");
        query.setParameter(1,from.getIdcity());
        List<Route> resultList = query.getResultList();
        if(resultList.isEmpty()){
            return null;
        }else {
            return resultList.get(0);
        }
    }

    @Override
    public Route findByFromAndToCity(City to, City from) {
        Query query =  em.createQuery("select r from Route r where r.from.idcity=?1 and r.to.idcity=?2");
        query.setParameter(1,from.getIdcity());
        query.setParameter(2,to.getIdcity());
        List<Route> routes = query.getResultList();
        if(!routes.isEmpty()){
            return routes.get(0);
        }
        return null;
    }

    @Override
    public Route findByFromAndToCity(Long to, Long from) {
        Query query =  em.createQuery("select r from Route r where r.from.idcity=?1 and r.to.idcity=?2");
        query.setParameter(1,from);
        query.setParameter(2,to);
        List<Route> routes = query.getResultList();
        if(!routes.isEmpty()){
            return routes.get(0);
        }
        return null;
    }

    @Override
    public Route createRoute(Route route) {
        em.persist(route);
        return route;
    }

    @Override
    public List<Long>  findIdsRoutesByIdCityFrom(Long idCityFrom){
        Query query = em.createQuery("select r.idroute as idroute from Route r where idcity_from=?1");
        query.setParameter(1,idCityFrom);
        return  new ArrayList<Long>(query.getResultList());
    }
}
