package org.project.core.services.exceptions;

/**
 * Created by swen on 5/6/16.
 */
public class CityNotExsistsException extends RuntimeException {
    public CityNotExsistsException() {
    }

    public CityNotExsistsException(String message) {
        super(message);
    }

    public CityNotExsistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
