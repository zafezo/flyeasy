package org.project.core.services.exceptions;

/**
 * Created by swen on 5/12/16.
 */
public class CartExsistException extends RuntimeException {
    public CartExsistException() {
    }

    public CartExsistException(String message) {
        super(message);
    }

    public CartExsistException(String message, Throwable cause) {
        super(message, cause);
    }
}
