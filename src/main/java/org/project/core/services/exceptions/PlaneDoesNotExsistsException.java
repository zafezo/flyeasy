package org.project.core.services.exceptions;

/**
 * Created by swen on 5/26/16.
 */
public class PlaneDoesNotExsistsException extends RuntimeException {
    public PlaneDoesNotExsistsException() {
    }

    public PlaneDoesNotExsistsException(String message) {
        super(message);
    }

    public PlaneDoesNotExsistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
