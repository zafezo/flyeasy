package org.project.core.services.exceptions;

/**
 * Created by swen on 5/10/16.
 */
public class CartNotExsistsException extends RuntimeException {
    public CartNotExsistsException() {
    }

    public CartNotExsistsException(String message) {
        super(message);
    }

    public CartNotExsistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
