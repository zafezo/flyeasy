package org.project.core.services.exceptions;

/**
 * Created by swen on 4/23/16.
 */
public class RounteNotFoundException extends RuntimeException {
    public RounteNotFoundException() {
    }

    public RounteNotFoundException(String message) {
        super(message);
    }

    public RounteNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RounteNotFoundException(Throwable cause) {
        super(cause);
    }

    public RounteNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
