package org.project.core.services.exceptions;

/**
 * Created by swen on 5/10/16.
 */
public class LineItemDoesNotExsistException extends RuntimeException {
    public LineItemDoesNotExsistException() {
    }

    public LineItemDoesNotExsistException(String message) {
        super(message);
    }

    public LineItemDoesNotExsistException(String message, Throwable cause) {
        super(message, cause);
    }
}
