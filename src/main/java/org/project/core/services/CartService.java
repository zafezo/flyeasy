package org.project.core.services;

import org.hibernate.criterion.LikeExpression;
import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.project.core.utils.CartList;
import org.project.core.utils.LineItemList;

import javax.sound.sampled.Line;

/**
 * Created by swen on 5/10/16.
 */
public interface CartService {
    Cart createCart(Cart cart);
    Cart deleteCart(Cart cart);
    Cart updateCart(Cart cart);
    Cart getCartById(Long cartId);
    CartList getAllCarts();
    LineItem addLineItem(Long cartId, LineItem item);
    LineItem removeLineItem(Long cartId, LineItem item);
    LineItemList getLineItemsByCartId(Long id);
}
