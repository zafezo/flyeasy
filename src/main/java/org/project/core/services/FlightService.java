package org.project.core.services;

import org.project.core.entinity.Flight;
import org.project.core.entinity.Route;
import org.project.core.utils.FlightList;
import org.project.rest.resources.SearchFlightResource;

import java.util.Date;
import java.util.List;

/**
 * Created by swen on 4/23/16.
 */
public interface FlightService{
    FlightList getAllFlight();

    Flight findById(Long id);

    Flight createFlight(Flight flight);
    Flight updateFlight(Long id,Flight flight);
    Flight deleteFlight(Long id);

    List<Flight> getByRouteAndStartDate(Route route , Date startDate, Date maxArrivalDate);
    List<Flight> getByCityFromIdAndStartDate(long from , Date startDate, Date maxArrivalDate);
    List<FlightList> getPossibleFlights(SearchFlightResource search);
}
