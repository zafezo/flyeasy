package org.project.core.services;

import org.project.core.entinity.Route;

/**
 * Created by swen on 5/6/16.
 */
public interface RouteService {
    Route getRouteByFromId(Long id);
}
