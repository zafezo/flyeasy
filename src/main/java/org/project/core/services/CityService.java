package org.project.core.services;

import org.project.core.entinity.City;
import org.project.core.utils.CityList;

import java.util.List;

/**
 * Created by swen on 5/6/16.
 */
public interface CityService {
    CityList getAllCities();
    City createCity(City City);
    City updateCity(City City);
    City deleteCity(Long id);
}
