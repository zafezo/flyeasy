package org.project.core.services;

import org.project.core.entinity.Plane;
import org.project.core.utils.PlaneList;

/**
 * Created by swen on 5/26/16.
 */
public interface PlaneService {

    PlaneList getAllPlanes();
    Plane createPlane(Plane plane);
    Plane updatePlane(Plane plane);
    Plane deletePlane(Long id);
}
