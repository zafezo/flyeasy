package org.project.core.services.impl;

import org.project.core.entinity.City;
import org.project.core.entinity.Route;
import org.project.core.services.exceptions.CityNotExsistsException;
import org.project.core.repositories.CityRepo;
import org.project.core.repositories.RouteRepo;
import org.project.core.services.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by swen on 5/6/16.
 */
@Service
@Transactional
public class RouteServiceImpl implements RouteService {



    @Autowired
    private RouteRepo routeRepo;

    @Autowired
    private CityRepo cityRepo;

    @Override
    public Route getRouteByFromId(Long id) {
        City city = cityRepo.findCity(id);
        if(city == null){
            throw  new CityNotExsistsException();
        }else{
            return  routeRepo.findByFromCity(city);
        }
    }
}
