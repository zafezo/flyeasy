package org.project.core.services.impl;

import org.project.core.entinity.LineItem;
import org.project.core.services.exceptions.LineItemDoesNotExsistException;
import org.project.core.repositories.LineItemRepo;
import org.project.core.services.LineItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by swen on 5/10/16.
 */
@Service
@Transactional
public class LineItemServiceImpl implements LineItemService {

    @Autowired
    private LineItemRepo lineItemRepo;

    @Override
    public LineItem createLineItem(LineItem item) {
        return lineItemRepo.createLineItem(item);
    }

    @Override
    public LineItem updateLineItem(LineItem item) {
        return lineItemRepo.updateLineItem(item);
    }

    @Override
    public LineItem deleteLineItem(LineItem item) {
        return lineItemRepo.deleteLineItem(item);
    }

    @Override
    public LineItem findById(Long id) {
        LineItem item = lineItemRepo.findById(id);
        if(item == null){
            throw  new LineItemDoesNotExsistException();
        }
        return item;
    }
}
