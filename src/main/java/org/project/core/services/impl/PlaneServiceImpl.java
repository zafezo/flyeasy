package org.project.core.services.impl;

import org.project.core.entinity.Plane;
import org.project.core.repositories.PlaneRepo;
import org.project.core.services.PlaneService;
import org.project.core.services.exceptions.PlaneDoesNotExsistsException;
import org.project.core.utils.PlaneList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by swen on 5/26/16.
 */
@Service
@Transactional
public class PlaneServiceImpl implements PlaneService{

    @Autowired
    private PlaneRepo planeRepo;

    @Override
    public PlaneList getAllPlanes() {
        return new PlaneList(planeRepo.findAll());
    }

    @Override
    public Plane createPlane(Plane plane) {
        return planeRepo.createPlane(plane);
    }

    @Override
    public Plane updatePlane(Plane plane) {
        return planeRepo.updatePlane(plane.getIdplane(), plane);
    }

    @Override
    public Plane deletePlane(Long id) {
        Plane plane = planeRepo.findPlane(id);
        if(plane == null) {
            throw  new PlaneDoesNotExsistsException("No plane with id " + id);
        }
        return planeRepo.deletPlane(id);
    }
}
