package org.project.core.services.impl;

import org.project.core.algo.FlightSearch;
import org.project.core.entinity.City;
import org.project.core.entinity.Flight;
import org.project.core.entinity.Route;
import org.project.core.services.exceptions.CityNotExsistsException;
import org.project.core.services.exceptions.FlightNotFoundException;
import org.project.core.repositories.CityRepo;
import org.project.core.repositories.FlightRepo;
import org.project.core.repositories.RouteRepo;
import org.project.core.services.FlightService;
import org.project.core.utils.FlightList;
import org.project.rest.resources.SearchFlightResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * Created by swen on 4/23/16.
 */
@Service
@Transactional
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightRepo flightRepo;

    @Autowired
    private RouteRepo routeRepo;

    @Autowired
    private CityRepo cityRepo;

    @Override
    public FlightList getAllFlight() {
        return new FlightList(flightRepo.findAllFlight());
    }

    @Override
    public Flight createFlight(Flight flight) {
        System.out.println(flight);
        if(flight.getRoute().getIdroute() == null){
            Route route = routeRepo.findByFromAndToCity(flight.getRoute().getTo(), flight.getRoute().getFrom());
            if(route == null) {
                route = routeRepo.createRoute(flight.getRoute());
            }
            flight.setRoute(route);
        };
        return flightRepo.createFlight(flight);
    }

    @Override
    public Flight findById(Long id) {
        Flight  flight = flightRepo.findById(id);
        if(flight != null){
            return flight;
        }else {
            throw  new FlightNotFoundException();
        }
    }

    @Override
    public Flight updateFlight(Long id, Flight flight) {
        if(flightRepo.findById(id) == null){
            throw new FlightNotFoundException("In updateFlight with flightID " + id);
        }
        return flightRepo.updateFlight(flight);
    }

    @Override
    public Flight deleteFlight(Long id) {
        if(flightRepo.findById(id) == null){
            throw new FlightNotFoundException("In updateFlight with flightID " + id);
        }
        Flight flight = findById(id);
        return flightRepo.deleteFlight(flight);
    }

    @Override
    public List<Flight> getByCityFromIdAndStartDate(long cityId, Date startDate, Date maxArrivalDate) {

        List<Long> routeIds= routeRepo.findIdsRoutesByIdCityFrom(cityId);

        List<Flight> flightList= new ArrayList<Flight>();

        for(Long routeId: routeIds)
        {
            Flight flight= flightRepo.findByRouteIdAndTimeInterval(routeId , startDate, maxArrivalDate);
            if(flight!=null){
                flightList.add(flight);
            }


        }

        return  flightList;
    }

    @Override
    public List<FlightList> getPossibleFlights(SearchFlightResource search) {
        Route route = routeRepo.findByFromAndToCity(search.getTo(), search.getFrom());
        Date date = search.getStartDate();
        //Get List of list of flight
        List<List<Flight>> returnedList = new FlightSearch(this).find(route, date);

        //Make correct result;
        List<FlightList> temp = new ArrayList<>();
        Iterator iterator = returnedList.iterator();
        while (iterator.hasNext()){
            temp.add(new FlightList((List<Flight>)iterator.next()));
        }
        return temp;
    }

    @Override
    public List<Flight> getByRouteAndStartDate(Route route ,Date startDate, Date maxArrivalDate){
        return flightRepo.findByRouteAndStartDate(route , startDate, maxArrivalDate);
    }

}
