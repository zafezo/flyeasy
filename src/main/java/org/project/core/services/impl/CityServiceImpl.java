package org.project.core.services.impl;

import org.project.core.entinity.City;
import org.project.core.repositories.CityRepo;
import org.project.core.services.CityService;
import org.project.core.services.exceptions.CityNotExsistsException;
import org.project.core.utils.CityList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swen on 5/6/16.
 */
@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepo cityRepo;

    @Override
    public CityList getAllCities() {
       return  new CityList(cityRepo.findAllCity());
    }

    @Override
    public City createCity(City City) {
        return cityRepo.createCity(City);
    }

    @Override
    public City updateCity(City City) {
        City tempCity = cityRepo.findCity(City.getIdcity());
        if(tempCity == null){
            throw new CityNotExsistsException("City not exist wit id" + City.getIdcity());
        }
        return cityRepo.updateCity(City.getIdcity(), City);
    }

    @Override
    public City deleteCity(Long id) {
        City tempCity = cityRepo.findCity(id);
        if(tempCity == null){
            throw new CityNotExsistsException("City not exist wit id" + id);
        }
        return cityRepo.deletCity(id);
    }
}
