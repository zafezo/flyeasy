package org.project.core.services.impl;

import org.project.core.entinity.Cart;
import org.project.core.entinity.LineItem;
import org.project.core.repositories.FlightRepo;
import org.project.core.services.exceptions.CartNotExsistsException;
import org.project.core.repositories.CartRepo;
import org.project.core.repositories.LineItemRepo;
import org.project.core.services.CartService;
import org.project.core.utils.CartList;
import org.project.core.utils.LineItemList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by swen on 5/10/16.
 */
@Service
@Transactional
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepo cartRepo;

    @Autowired
    private LineItemRepo lineItemRepo;

    @Autowired
    private FlightRepo flightRepo;

    @Override
    public Cart createCart(Cart sendedCart) {

        return cartRepo.createCart(sendedCart);
    }

    @Override
    public Cart deleteCart(Cart cart) {
        return cartRepo.deleteCart(cart);
    }

    @Override
    public Cart updateCart(Cart cart) {
        return cartRepo.updateCart(cart);
    }

    @Override
    public Cart getCartById(Long cartId) {
        Cart cart = cartRepo.findById(cartId);
        if(cart == null){
            throw  new CartNotExsistsException();
        }
        return cart;
    }

    @Override
    public CartList getAllCarts() {
        return new CartList(cartRepo.findAllCarts());
    }

    @Override
    public LineItem addLineItem(Long cartId, LineItem item) {
        Cart cart = cartRepo.findById(cartId);
        if(cart == null){
            throw  new CartNotExsistsException();
        }else {
            cart.getLineItems().add(item);
            item.setFlight(flightRepo.findById(item.getFlight().getIdflight()));
            return item;
        }
    }

    @Override
    public LineItem removeLineItem(Long cartId, LineItem item) {
        Cart cart = cartRepo.findById(cartId);
        if(cart == null){
            throw  new CartNotExsistsException();
        }else {
            cart.getLineItems().remove(item);
            return item;
        }
    }

    @Override
    public LineItemList getLineItemsByCartId(Long id) {
        Cart cart = cartRepo.findById(id);
        if(cart != null){
            return new LineItemList(lineItemRepo.findByCartId(id));
        }else {
            throw new CartNotExsistsException();
        }
    }
}
