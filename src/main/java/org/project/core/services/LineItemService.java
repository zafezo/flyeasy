package org.project.core.services;

import org.project.core.entinity.LineItem;

/**
 * Created by swen on 5/10/16.
 */
public interface LineItemService {
    LineItem createLineItem(LineItem item);
    LineItem updateLineItem(LineItem item);
    LineItem deleteLineItem(LineItem item);
    LineItem findById(Long id);
}
