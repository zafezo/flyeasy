package org.project.core.entinity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by swen on 4/19/16.
 */
@Entity
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue
    private Long idcart;

    private String first_name;
    private String last_name;
    private String email;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cart_lineItem",
            joinColumns = { @JoinColumn(name = "cart_id") },
            inverseJoinColumns = { @JoinColumn(name = "lineItem_id")})
    private Set<LineItem> lineItems = new HashSet<>();


    public Set<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(Set<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public Long getIdcart() {
        return idcart;
    }

    public void setIdcart(Long idcart) {
        this.idcart = idcart;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "idcart=" + idcart +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", lineItems=" + lineItems +
                '}';
    }
}
