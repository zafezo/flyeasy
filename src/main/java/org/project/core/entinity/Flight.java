package org.project.core.entinity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by swen on 4/18/16.
 */
@Entity
@Table(name = "flight")
public class Flight implements Serializable {

    @Id
    @GeneratedValue
    private Long idflight;

    @ManyToOne
    @JoinColumn(name = "idplane")
    private Plane plane;

    @ManyToOne
    @JoinColumn(name = "idroute")
    private Route route;

    private Date arriveDate;
    private Date  departureDate;

    public Long getIdflight() {
        return idflight;
    }

    public void setIdflight(Long idflight) {
        this.idflight = idflight;
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "idflight=" + idflight +
                ", plane=" + plane +
                ", route=" + route +
                ", arriveDate=" + arriveDate +
                ", departureDate=" + departureDate +
                '}';
    }
}
