package org.project.core.entinity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by swen on 4/18/16.
 */
@Entity
@Table(name = "plane")
public class Plane implements Serializable{

    @Id
    @GeneratedValue
    private Long idplane;

    private String name;

    private int seats_count;

    public Plane(){}

    public Plane(String name, int seats_count) {
        this.name = name;
        this.seats_count = seats_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeats_count() {
        return seats_count;
    }

    public void setSeats_count(int seats_count) {
        this.seats_count = seats_count;
    }

    public Long getIdplane() {
        return idplane;
    }

    public void setIdplane(Long idplane) {
        this.idplane = idplane;
    }
}

