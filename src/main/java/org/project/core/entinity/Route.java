package org.project.core.entinity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by swen on 4/18/16.
 */
@Entity
@Table(name = "route")
public class Route implements Serializable {

    @Id
    @GeneratedValue
    private Long idroute;

    @ManyToOne
    @JoinColumn(name = "idcity_from")
    private City from;

    @ManyToOne
    @JoinColumn(name = "idcity_to")
    private City to;

    private float price;

    public Route(){};

    public Long getIdroute() {
        return idroute;
    }

    public void setIdroute(Long idroute) {
        this.idroute = idroute;
    }

    public City getFrom() {
        return from;
    }

    public void setFrom(City from) {
        this.from = from;
    }

    public City getTo() {
        return to;
    }

    public void setTo(City to) {
        this.to = to;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Route{" +
                "idroute=" + idroute +
                ", from=" + from +
                ", to=" + to +
                ", price=" + price +
                '}';
    }
}
