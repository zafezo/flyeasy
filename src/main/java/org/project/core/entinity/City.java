package org.project.core.entinity;

import com.fasterxml.jackson.annotation.JsonSubTypes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by swen on 4/18/16.
 */
@Entity
@Table(name = "city")
public class City implements Serializable{

    @Id
    @GeneratedValue
    private Long idcity;

    private String name;

    public City(){};

    public City(String name) {
        this.idcity = Long.valueOf(name);
    }

    public Long getIdcity() {
        return idcity;
    }

    public void setIdcity(Long idcity) {
        this.idcity = idcity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "City{" +
                "idcity=" + idcity +
                ", name='" + name + '\'' +
                '}';
    }
}
