package org.project.core.entinity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by swen on 4/19/16.
 */
@Entity
@Table(name = "lineItem")
public class LineItem {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long idorder;
    private int quantity;

    @ManyToOne()
    @JoinColumn(name = "idflight")
    private Flight flight;

    @ManyToMany(mappedBy = "lineItems")
    private Set<Cart> carts = new HashSet<>();

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Long getIdorder() {
        return idorder;
    }

    public void setIdorder(Long idorder) {
        this.idorder = idorder;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
