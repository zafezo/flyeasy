/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'ngBoilerplate.home', [
      'ui.router',
      'ngResource',
      'ngBoilerplate.cart',
      'ngBoilerplate.city'
    ])
    .config(function config( $stateProvider ) {
      $stateProvider.state( 'home', {
        url: '/home',
        views: {
          "main": {
            controller: 'HomeCtrl',
            templateUrl: 'home/home.tpl.html'
          }
        },
        data:{ pageTitle: 'Home' },
        resolve:{
          cities:function (CityService) {
            return CityService.getAllCities().then(function (data){
              return data.cities;
            });
          }
        }
      });
    })
    .factory('searchService',function ($resource) {
      var service = {};
      service.getByCities = function (data) {
        var Search = $resource('/business-airlines/rest/flights/search');
        return Search.save(data).$promise;
      };
      return service;
    })

    .controller( 'HomeCtrl', function HomeController( $scope,cities,searchService,$state,cartService) {
          $scope.cities = cities;
      $scope.searchForm = {};
      $scope.searchForm.quantity = 1;
      $scope.search = function () {
        var requestData = {
          to:$scope.searchForm.to,
          from:$scope.searchForm.from,
          startDate:$scope.searchForm.startDate
        };
        searchService.getByCities(requestData).then(function (response) {
          $scope.results = response.result;
        });
      };
      $scope.addToCart = function (flights) {
        flights.forEach(function (item, index) {
          item.quantity = $scope.searchForm.quantity || 1;
          console.log(1);
          cartService.addToCart(item);
        });
        $state.go('cart');
      };
    })
;
