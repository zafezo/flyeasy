angular.module( 'ngBoilerplate.cart', [
        'ui.router',
        'placeholders',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'cart', {
                url: '/cart',
                views: {
                    "main": {
                        controller: 'CartCtrl',
                        templateUrl: 'cart/cart.tpl.html'
                    }
                },
                data:{ pageTitle: 'Cart' }
            })
            .state('entryData', {
                url: '/cart/buy',
                views: {
                    "main": {
                        controller:"EntryDataCartCtrl",
                        templateUrl:'cart/enterData.tpl.html'
                    }
                },
                data:{pageTitle:'Cart'}
            })
        ;
    })
    .factory('cartApi',function ($resource) {
        var service ={};

        service.getAllCarts =  function () {
            var Cart = $resource('/business-airlines/rest/cart');
            return Cart.get().$promise;
        };
        service.getAllLineItemsByCartId =  function (cartId) {
            var Cart = $resource('/business-airlines/rest/cart/:id');
            return Cart.get({id: cartId}).$promise;
        };
        service.addLineItems = function (cartId,lineItems, success, failure) {
            var Cart = $resource('/business-airlines/rest/cart/:cartId/items');
            return Cart.save({cartId:cartId},{lineItems:lineItems},success,failure);
        };
        service.addLineItem = function (cartId,lineItem) {
            var Cart = $resource('/business-airlines/rest/cart/:cartId/item');
            return Cart.save({cartId:cartId},lineItem).$promise.then(function (data) {
                return data.lineItems;
            });
        };
        service.createCart = function (cartData, success, failure) {
            var Cart = $resource('/business-airlines/rest/cart/');
            return Cart.save({},cartData,success,failure);
        };
        return service;
    })
    .factory('cartService',function () {
        var service = {};
        service.addToCart = function (data) {
            var cart = JSON.parse(localStorage.getItem("lineItems"));
            cart = cart || [];
            cart.push(data);
            localStorage.setItem("lineItems",JSON.stringify(cart));
        };
        service.getCart = function () {
            return JSON.parse(localStorage.getItem("lineItems")) || [];
        };
        service.getLineItems = function () {
            var data = [];
            JSON.parse(localStorage.getItem("lineItems")).forEach(function(element, index, array){
                data.push({flightId:element.rid, quantity:element.quantity});
            });
            return data;
        };
        service.removeElemet = function (data) {
            var list = JSON.parse(localStorage.getItem("lineItems"));
            for (var i = 0; i < list.length; i++) {
                if (list[i].rid === data.rid) {
                    list.splice(i, 1);
                }
            }
            localStorage.setItem("lineItems",JSON.stringify(list));
        };
        service.clearCart = function () {
            localStorage.setItem("lineItems",JSON.stringify([]));
        };
        return service;
    })
    .controller( 'CartCtrl', function CartCtrl( $scope, cartService,$state ) {
        $scope.items = cartService.getCart();
        $scope.totalPrice = function () {
            if($scope.items){
                return sum =  $scope.items.reduce(function(sum, current) {
                    return sum + parseFloat(current.price)*parseFloat(current.quantity);
                },0);
            }
            return 0;
        };
        $scope.remove = function (data) {
            cartService.removeElemet(data);
            $state.go('cart',{},{reload:true});
        };
        $scope.clear = function () {
            cartService.clearCart();
            $state.go('cart',{},{reload:true});
        };
    })
    .controller( 'EntryDataCartCtrl', function ( $scope, cartService,$state, cartApi ) {
        $scope.data = {};
        $scope.buy = function () {
            cartApi.createCart($scope.data,
                function (responseData) {
                    cartApi.addLineItems(responseData.rid, cartService.getLineItems(),
                        function (response) {
                            $scope.items = response.lineItems;
                        },
                        function () {
                            alert("Some errors!");
                        }
                    );
                },function () {
                    alert("Some errors!");
                });
        };
    })

;