angular.module( 'ngBoilerplate.city', [
        'ui.router',
        'ngResource'
    ])
    .config(function ($stateProvider) {
        $stateProvider
            .state('cities', {
                url: '/admin/cities',
                views: {
                    "main": {
                        controller: 'CityCtrl',
                        templateUrl: 'city/city.tpl.html'
                    }
                },
                data:{ pageTitle: 'Admin Panel | City Panel' },
                resolve: {
                    cities:  function (CityService) {
                        return CityService.getAllCities().then(function (data){
                            return data.cities;
                        });
                    }
                }
            });
    })
    .factory('CityService', function ($resource) {
        var service = {};
        service.getAllCities =  function () {
            var City = $resource('/business-airlines/rest/cities');
            return City.get().$promise;
        };
        service.createCity = function (cityData, success, failure) {
            var City = $resource('/business-airlines/rest/cities/');
            return City.save({},cityData,success,failure);
        };
        service.updateCity = function (cityData, success, failure) {
            var City = $resource('/business-airlines/rest/cities/:id');
            return City.save({id:cityData.rid},cityData,success,failure);
        };
        service.deleteCity = function (cityData, success) {
            var City = $resource('/business-airlines/rest/cities/:id');
            return City['delete']({id:cityData.rid},success);
        };
        return service;
    })
    .controller('CityCtrl',function ($scope, CityService,$state,cities) {
        $scope.cities = cities;
        $scope.save = function () {
            CityService.createCity($scope.city, function (data) {
                $scope.cities.push(data);
            },function () {
                alert("Error with create " + $scope.city.name);
            });
        };
        $scope.updateClick = function (plane) {
            $scope.updating = true;
            $scope.updatedCity = plane;
        };
        $scope.update = function () {
            CityService.updateCity($scope.updatedCity, function (data) {
            },function () {
                alert("Error with update with id  " + $scope.updatedCity.rid);
            });
            $scope.updating = false;
        };
        $scope.removeCity = function (city) {
            CityService.deleteCity(city, function (response) {
                $state.go('cities',{},{reload:true});
            });
        };
    })
;
