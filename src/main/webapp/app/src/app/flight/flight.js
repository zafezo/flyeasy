angular.module( 'ngBoilerplate.flight', [
        'ui.router',
        'ngResource',
        'ngBoilerplate.plane',
        'ngBoilerplate.city'

    ])
    .config(function ($stateProvider) {
        $stateProvider
            .state('flights', {
                url: '/admin/flights',
                views: {
                    "main": {
                        controller: 'FlightsCtrl',
                        templateUrl: 'flight/flight.tpl.html'
                    }
                },
                data:{ pageTitle: 'Admin Panel | Flight Panel' },
                resolve: {
                    cities:function (CityService) {
                        return CityService.getAllCities().then(function (data){
                            return data.cities;
                        });
                    },
                    flights:  function (FlightService) {
                        return FlightService.getAllFlights().then(function (data){
                            return data.flights;
                        });
                    },
                    planes:  function (PlaneService) {
                        return PlaneService.getAllPlanes().then(function (data){
                            return data.planes;
                        });
                    }
                }
            })
        ;
    })
.factory('FlightService', function ($resource) {
    var service = {};
    service.getAllFlights =  function () {
        var Flight = $resource('/business-airlines/rest/flights');
        return Flight.get().$promise;
    };
    service.createFlight = function (flightData, success, failure) {
        var Flight = $resource('/business-airlines/rest/flights/');
        return Flight.save({},flightData,success,failure);
    };
    service.updateFlight = function (flightData, success, failure) {
        var Flight = $resource('/business-airlines/rest/flights/:id');
        return Flight.save({id:flightData.rid},success,failure);
    };
    service.deleteFlight = function (flightData, success) {
        var Flight = $resource('/business-airlines/rest/flights/:id');
        return Flight['delete']({id:flightData.rid},success);
    };
    return service;
})
.controller('FlightsCtrl',function ($scope, FlightService,$state,flights, cities, planes) {
    $scope.planes = planes;
    $scope.flights = flights;
    $scope.cities = cities;
    $scope.save = function () {
        FlightService.createFlight($scope.flight, function (data) {
            $scope.flights.push(data);
        },function () {
            alert("Error with create " + $scope.flights.name);
        });
    };
    $scope.updateClick = function (flight) {
        $scope.updating = true;
        $scope.updatedFlight = flight;

    };
    $scope.update = function () {
        for(var i = 0; i<$scope.cities.length; i++){
            if($scope.updatedFlight.to === $scope.cities[i].name){
                $scope.updatedFlight.to = "" + $scope.cities[i].rid;
            }
            if($scope.updatedFlight.from === $scope.cities[i].name){
                $scope.updatedFlight.from = "" + $scope.cities[i].rid;
            }
        }
        console.log($scope.updatedFlight);
        FlightService.updateFlight($scope.updatedFlight, function (data) {
            $state.go('flights',{},{reload:true});
        });
        $scope.updating = false;
    };
    $scope.removeFlights = function (flight) {
        FlightService.deleteFlight(flight, function (response) {
            $state.go('flights',{},{reload:true});
        });
    };
})
;
