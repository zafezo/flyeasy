angular.module('ngBoilerplate.login',[
        'ui.router'
])
    .config(function config( $stateProvider ) {
        $stateProvider.state( 'login', {
                url: '/login',
                views: {
                    "main": {
                        controller: 'LoginCtrl',
                        templateUrl: 'login/login.tpl.html'
                    }
                },
                data:{ pageTitle: 'Login' }
            });
    })
.factory('loginService',function ($state) {
    var service = {};
    service.login = function (data) {
        var login = JSON.parse(localStorage.getItem("login"));
        if(data = "admin"){
            login = "true";
            localStorage.setItem("login",JSON.stringify(login));
            $state.go("flights");
        }
    };

    service.isLogged = function () {
        return JSON.parse(localStorage.getItem("login")) === "true";
    };

    service.logout = function () {
        localStorage.setItem("login",JSON.stringify(null));
        $state.go("home");
    };
    return service;
})
    .controller( 'LoginCtrl', function ( $scope, loginService) {
       $scope.loginService = loginService;
    });
