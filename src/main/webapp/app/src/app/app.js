angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
    'ngBoilerplate.plane',
    'ngBoilerplate.city',
      'ngBoilerplate.cart',
    'ngBoilerplate.flight',
      'ngBoilerplate.home',
    'ngBoilerplate.login',
      'ngBoilerplate.lineItem',
  'ui.router'
])
    .run(function ($rootScope, $state, loginService) {
      $rootScope.$on('$stateChangeStart', function(event, toState){

        if(!loginService.isLogged()){
          if ((toState.name !== 'cart' && toState.name !== 'entryData' && toState.name !== 'home' && toState.name !== 'login')){
            event.preventDefault();
            $state.go('login');
          }
        }
      });
    })
.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location, loginService ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle ;
    }
  });
  $scope.loginService = loginService;
})

;

