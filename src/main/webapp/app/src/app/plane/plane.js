angular.module( 'ngBoilerplate.plane', [
        'ui.router',
    'ngResource'
    ])
    .config(function ($stateProvider) {
        $stateProvider
            .state('planes', {
                url: '/admin/planes',
                views: {
                    "main": {
                        controller: 'PlaneCtrl',
                        templateUrl: 'plane/plane.tpl.html'
                    }
                },
                data:{ pageTitle: 'Admin Panel | Plane Panel' },
                resolve: {
                    planes:  function (PlaneService) {
                        return PlaneService.getAllPlanes().then(function (data){
                            return data.planes;
                        });
                    }
                }
            });
    })
    .factory('PlaneService', function ($resource) {
        var service = {};
        service.getAllPlanes =  function (fun) {
            var Planes = $resource('/business-airlines/rest/planes');
            return Planes.get().$promise;
        };
        service.createPlane = function (planeData, success, failure) {
            var Planes = $resource('/business-airlines/rest/planes/');
            return Planes.save({},planeData,success,failure);
        };
        service.updatePlane = function (planeData, success, failure) {
            var Planes = $resource('/business-airlines/rest/planes/:id');
            return Planes.save({id:planeData.rid},planeData,success,failure);
        };
        service.deletePlane = function (planeData, success) {
            var Planes = $resource('/business-airlines/rest/planes/:id');
            return Planes['delete']({id:planeData.rid},success);
        };
        return service;
    })
    .controller('PlaneCtrl',function ($scope, PlaneService,$state,planes) {
        $scope.planes = planes;
        $scope.save = function () {
            PlaneService.createPlane($scope.plane, function (data) {
                $scope.planes.push(data);
            },function () {
                alert("Error with create " + $scope.plane.name);
            });
        };
        $scope.updateClick = function (city) {
          $scope.updating = true;
          $scope.updatedPlane = city;
        };
        $scope.update = function () {
            PlaneService.updatePlane($scope.updatedPlane, function (data) {
            },function () {
                alert("Error with update with id  " + $scope.updatedPlane.rid);
            });
            $scope.updating = false;
        };
        $scope.removePlane = function (plane) {
            PlaneService.deletePlane(plane, function (response) {
                $state.go('planes',{},{reload:true});
            });
        };
    });