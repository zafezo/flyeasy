angular.module( 'ngBoilerplate.lineItem', [
        'ui.router'
    ])
    .config(function ($stateProvider) {
        $stateProvider
            .state('lineItem', {
                url: '/admin/lineItem',
                views: {
                    "main": {
                        controller: 'LineItemCtrl',
                        templateUrl: 'lineItem/lineItem.tpl.html'
                    }
                },
                data:{ pageTitle: 'Admin Panel | Line Item Panel' },
                resolve: {
                    carts:  function (cartApi) {
                        return cartApi.getAllCarts().then(function (data){
                            return data.carts;
                        });
                    }
                }
            });
    })
    .controller('LineItemCtrl',function ($scope, cartApi,$state,carts) {
        $scope.carts = carts;
        $scope.showLineItems = function (carId) {
            cartApi.getAllLineItemsByCartId(carId).then(function (data) {
              $scope.lineItems = data.lineItems;
                $scope.carId = carId;
            });
        };
    })
;
