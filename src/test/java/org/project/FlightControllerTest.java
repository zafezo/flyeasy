package org.project;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.project.core.services.FlightService;
import org.project.rest.controllers.FlightController;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by swen on 4/23/16.
 */
public class FlightControllerTest {

    @InjectMocks
    private FlightController flightController;

    @Mock
    private FlightService flightService;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(flightController).build();
    }

//    @Test
//    public void getSpecificFlights(){
//        String from = "Rzeszów";
//        String to = "Warszawa";
//        String data = "1993-02-03";
////        Date date = new Date(data);
//        try {
//            mockMvc.perform(post("/rest/specific-flights")
//                            .param("from",from)
//                            .param("to",to)
////                            .param("date",data)
//            )
//                    .andDo(print())
//                    .andExpect(status().isOk());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
